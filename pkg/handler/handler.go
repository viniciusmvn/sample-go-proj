package handler

import (
	"fmt"
	"net/http"
)

type Service interface {
	Do()
}

func New(svc Service) *hdlr {
	return &hdlr{service: svc}
}

type hdlr struct {
	service Service
}

func (h hdlr) Handle(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hello", r.URL.Path)
}
