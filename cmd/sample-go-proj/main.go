package main

import (
	"database/sql"
	"net/http"

	"gitlab.com/b1217/sample-go-proj/pkg/handler"
	"gitlab.com/b1217/sample-go-proj/pkg/repository"
	"gitlab.com/b1217/sample-go-proj/pkg/service"
)

func main() {
	db, err := sql.Open("postgres", "postgresql://")
	if err != nil {
		panic(err)
	}
	repo := repository.Repository{db}
	svc := service.Service{Repo: repo}
	hdlr := handler.New(svc)
	http.HandleFunc("/", hdlr.Handle)
	http.ListenAndServe(":9090", nil)
}
